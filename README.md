#monogame-vb
Template to help get you started with MonoGame with VB.Net

##Compile
```
vbnc /r:MonoGame.Framework.dll App.vb Game.vb
```

##Execute
```
mono App.exe
```